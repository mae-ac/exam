import math
from typing import List, Dict, Tuple
from helpers import time_to_ms, ms_to_time


def get_number_of_records(data) -> int:
    return -1


def get_tracks(data) -> List[str]:
    return []


def get_fastest_track(data) -> str:
    return ""


def get_number_of_records_by_player(data) -> Dict[str, int]:
    return {}


def get_player_with_most_records(data) -> str:
    return ""


def get_most_common_characters_without_hc(data) -> List[Tuple[str, int]]:
    return []


if __name__ == "__main__":
    from datasets import MARIO_KART_WORLD_RECORDS_DATASET
    import matplotlib.pyplot as plt
    from helpers import mu_vline, sigma_vlines, time_to_ms, ms_to_time

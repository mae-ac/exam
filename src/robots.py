from typing import Callable, TypeVar, List, Tuple, Dict

from path_finding import dijkstra, a_star

# These constants are used to represent the different types of robots and resources
# You can use these constants to index into the lists that represent the state of the factory
# Ex: state.robots[IRON] or self.robots[IRON] would give you the number of iron robots in the factory
# Ex: state.resources[IRON] or self.resources[IRON] would give you the number of iron resources in the factory
IRON: int = 0
COPPER: int = 1
SILICON: int = 2
URANIUM: int = 3

# This is a dictionary that maps the names of the robots to the index of the robot in the list
# Ex: ROBOT_NAMES_TO_INDICES['ironRobot'] would give you 0
# By using this dictionary, you can refer to the robots by name instead of by index
ROBOTS_NAME_TO_INDICES: Dict[str, int] = {
    'ironRobot': IRON,
    'copperRobot': COPPER,
    'siliconRobot': SILICON,
    'uraniumRobot': URANIUM
}


class Factory:
    # add your constructor and the other methods here

    def __lt__(self, other) -> bool:
        return True

    def __eq__(self, other) -> bool:
        return True

    def __hash__(self) -> int:
        h = hash(str(self.resources) + str(self.robots) + str(self.step))
        return h

    def __repr__(self):
        return f'Factory(robots={self.robots}, resources={self.resources}, step={self.step})'


def get_neighbors(state: Factory) -> List[Tuple[Factory, float]]:
    return [(state, 1.0)]


if __name__ == "__main__":
    # You can test your implementation here
    pass
